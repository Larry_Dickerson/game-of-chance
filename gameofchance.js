/*Reference for this assessment goes to "Traversy Media" channel
youtube, video titled "Rock Paper Scissors Game - UI & Javascript,
posted on June, 20, 2019, at https://www.youtube.com/watch?v=WR_pWXJZiRY" */
const choices = document.querySelectorAll('.choice');
const score = document.getElementById('score');
const result = document.getElementById('result'); 
const restart = document.getElementById('restart');
const modal = document.querySelector('.modal');
const scoreboard = {
    player: 0,
    computer: 0
}

// gameplay function
function play(evt) {
    restart.style.display = 'inline-block';
    const playerChoice = evt.target.id;
    const computerChoice = getComputerChoice();
    const winner = getWinner(playerChoice, computerChoice);
    displayWinner(winner, computerChoice);
}

// get computer's choice based on randomness!
function getComputerChoice() {
    const randomNum = Math.random();
    if (randomNum < 0.34) {
        return 'rock';
    } else if (randomNum <= 0.67) {
        return 'paper';
    } else {
        return 'scissors';
    }
}
// compare function to determine winner!
function getWinner(player, computer) {
    if (player === computer) {
        return 'draw';
    } else if (player === 'rock') {
        if (computer === 'paper') {
            return 'computer';
        } else {
            return 'player';
        }
    } else if (player === 'paper') {
        if (computer === 'scissors') {
            return 'computer';
        } else {
            return 'player';
        }
    } else if (player === 'scissors') {
        if (computer === 'rock') {
            return 'computer';
        } else {
            return 'player';
        }
    }
}
// function to display winner results
function displayWinner(winner, computerChoice) {
    if (winner === 'player') {
        // increment the player's score
        scoreboard.player++;
        // show the modal result
        result.innerHTML = `
            <h1 class="text-win">You Win!</h1>
            <img src="rock-paper-scissors-156171_640.png">
            <p>Computer Chose <strong>${computerChoice.charAt(0).toUpperCase() + computerChoice.slice(1)}</strong>!</p>
        `;
    } else if (winner === 'computer') {
        // increment the computer's score
        scoreboard.computer++;
        // show the modal result
        result.innerHTML = `
            <h1 class="text-lose">You Lose!</h1>
            <img src="rock-paper-scissors-156171_640.png">
            <p>Computer Chose <strong>${computerChoice.charAt(0).toUpperCase() + computerChoice.slice(1)}</strong>!</p>
        `;
    } else {
        result.innerHTML = `
            <h1>It's a Draw!</h1>
            <img src="rock-paper-scissors-156171_640.png">
            <p>Computer Chose <strong>${computerChoice.charAt(0).toUpperCase() + computerChoice.slice(1)}</strong>!</p>
        `;
    }
    // Show the score!
    score.innerHTML = `
    <p>Player: ${scoreboard.player}</p>
    <p>Computer: ${scoreboard.computer}</p>
    `;

    modal.style.display = 'block';
}

// Restart game function
function restartGame() {
    scoreboard.player = 0;
    scoreboard.computer = 0;
    score.innerHTML = `
    <p>Player: 0</p>
    <p>Computer: 0</p>
    `;
}

// Clear modal function
function clearModal(evt) {
    if (evt.target === modal) {
        modal.style.display = 'none';
    }
}

// the event listeners using for each
choices.forEach(choice => choice.addEventListener('click', play));
// add event listener for the window object
window.addEventListener('click', clearModal);
// add event listener for the restart button
restart.addEventListener('click', restartGame);